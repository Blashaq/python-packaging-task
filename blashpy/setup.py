from setuptools import setup
from setuptools import find_packages

setup(
    name='blashpy',
    version='0.9',
    packages=find_packages(),
    license='MIT',
    description='DPP package',
    long_description=open("README.txt").read(),
    install_requires=['lxml', 'requests'],
    author='Adrian Blasiak',
    author_email='212718@student.pwr.edu.pl',
    url='http://pypi.python.org/'
)
