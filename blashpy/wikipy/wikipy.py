import urllib.request
import json
from urllib.error import HTTPError

from lxml import html
import re


def get_articles(search_token):
    wiki_url = 'https://en.wikipedia.org/w/api.php?action=query&list=search&srsearch=' + search_token + '&format=json'

    with urllib.request.urlopen(wiki_url) as url:
        data = json.loads(url.read().decode())
        return data['query']


def get_article_header(article_id):
    wiki_url = 'https://en.wikipedia.org/?curid=' + str(article_id)
    try:
        with urllib.request.urlopen(wiki_url) as url:
            tree = html.fromstring(url.read().decode())

            origin_node = tree.xpath('//*[@id="mw-content-text"]/div/p[1]//text()')

            origin_text = ""
            for elem in origin_node:
                origin_text += elem
            origin_text = re.sub('\[(.*?)\]', "", origin_text)
            origin_text = origin_text.replace("\xa0", "")
            return origin_text
    except HTTPError:
        return "brak danych!"
