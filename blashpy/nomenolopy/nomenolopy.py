from lxml import html
import requests


def origin(name):
    page = requests.get('http://www.ksiegaimion.com/' + name)
    tree = html.fromstring(page.content)

    origin_node = tree.xpath('//*[@id="content"]/div[2]/div[2]/p//text()')

    origin_text = ""
    for elem in origin_node:
        origin_text += elem
    return origin_text or "nie ma takiego imienia w bazie!"


def meaning(name):
    page = requests.get('http://www.ksiegaimion.com/' + name)
    tree = html.fromstring(page.content)

    meaning_node = tree.xpath('//*[@id="content"]/p[1]//text()"')

    meaning_text = ""
    for elem in meaning_node:
        meaning_text += elem
    return meaning_text or "nie ma takiego imienia w bazie!"
